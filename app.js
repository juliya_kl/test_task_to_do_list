const newTaskNameInput=document.getElementById("new_task");
const tasksListUl=document.getElementById("tasks_list");

const getNewTaskElement = function (taskName) {
    const liTask=document.createElement("li");

    const checkBox=document.createElement("input");
    checkBox.type="checkbox";
    checkBox.onclick = markTask;

    const taskNameSpan=document.createElement("span");
    taskNameSpan.innerText=taskName;
    taskNameSpan.className = "incompleted";

    liTask.appendChild(checkBox);
    liTask.appendChild(taskNameSpan)
    return liTask;
}

const addNewTask = function () {
    if (newTaskNameInput.value) {
        tasksListUl.appendChild(getNewTaskElement(newTaskNameInput.value));
        newTaskNameInput.value="";
    }
}

const markTask = function () {
    const span = this.parentNode.childNodes.item(1);
    span.className === "incompleted" ? span.className = "completed" : span.className = "incompleted";
}
